<!DOCTYPE hmtl>
<html>
	<head>
		<title>Ejercicio 6</title>
		<script type="text/javascript" src="../js/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="../css/estilo.css">
	</head>
	<body>
		<table id="tabla">
			<caption>Acceso al Sistema</caption>
			<tr>
				<th>Usuario:</th>
				<td><input type="text" id="usuario" name="usuario" size="30" maxlength="20" value=""/></td>
			</tr>
			<tr>
				<th>Clave</th>
				<td><input type="password" id="clave" name="clave" size="30" maxlength="20" value=""/></td>
			</tr>
			<tr>
				<td colspan="2" class="centrado"><button type="button" onclick="ingresar()">Ingresar</button></td>
			</tr>
		</table>
		<div id="ajax-rs" class="centrado" ></div> 
	</body>
	<script>
		function ingresar(){
			var datos={"usuario":$("#usuario").val(),
					   "clave":$("#clave").val()};
					   
			$.ajax({
				data: datos,
				url:  "acceso-procesar.php",
				type: "post",
				success: function(response){
					$("#ajax-rs").html(response);
				}
			});
		}
		
		$("#usuario").focus();
	</script>
</html>

