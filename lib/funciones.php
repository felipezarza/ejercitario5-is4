<?php
	function fn_conectar(){
		$host="localhost";
		$dbname="productos2020";
		$user="postgres";
		$pass="postgres";
		
		$cmd = "pgsql:host=$host;dbname=$dbname;";
				
		try{
			$conn = new PDO($cmd,$user,$pass);
			return $conn;
		}catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}
	}
	
	function fn_desconectar($conn){
		$conn = null;
	}
	
	function fn_ejecutar_sql($sentencia,$conn){
		$sql = $conn->prepare($sentencia);
		$sql->execute();
		try{
			$rs = $sql->fetchAll();
			return $rs;
		}catch(PDOException $e)
		{
			echo "Error: " . $e->getMessage();
		}			
	}
	
	function fn_listar_combo($name,$sql,$fk=0,$evento='',$conn)
	{
		if(!isset($fk)) $fk=0;
		
		$salida = "<select id='$name' name='$name' onchange='$evento'><option value='' selected></option>";
		
		$result = fn_ejecutar_sql($sql,$conn);
		foreach($result as $registro)
		{
			$salida .= "<option value='{$registro[0]}'>{$registro[1]}</option>";
		}
		
		$salida .= "</select>";
		
		echo $salida;
	}
?>